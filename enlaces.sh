#!/bin/sh
# Arregla enlaces en sitio de desarrollo.
# Public domain. 2006

op=$1;

cmd="ln -s"
if (test "$op" = "-cvs") then {
	cmd="cp"
} fi;

if (test "$DCONFSH" = "") then {
	DCONFSH=$HOME/structio/devel/confsh;
} fi;

if (test ! -f $DCONFSH/conf.sh) then {
	echo "Falta ruta de confsh en variable DCONFSH";
	exit 1;
} fi;

function noFuentes {
	r=$1;
	touch $r/ploc.ploc
	cp $r/ploc.ploc ploc.ploc 2> /dev/null
	if (test "$?" != "0") then {
		echo 'Omitiendo operación sobre los mismos archivos';
		rm ploc.ploc
		exit 0;
	} fi;
	rm ploc.ploc
	rm $r/ploc.ploc
}

function opera {
	f=$1;
	d=$2;
	diff $f $d > /dev/null
	if (test -f $f -a -f $d -a "$?" != "0") then {
		echo "No se opera por diferencias entre $d $f";
	} 
	else {
		rm -f $d;
		eval "$cmd $f $d"
	} fi;
}

for i in Derechos.txt Creditos.txt Instala.txt Novedades.txt Leame.txt Tareas.txt ; do
	if (test ! -f $i) then {
		echo "Falta archivo $i";
	} fi;
done;

vc=`grep '^[ ]*[A-Z][_A-Z]*[ ]*=[ ]*".*"' $DCONFSH/confv.empty | sed -e 's/^[ ]*\([A-Z][_A-Z]*\).*/\1/g'`
for i in $vc; do
	grep "^[ ]*$i" confv.empty > /dev/null
	if (test "$?" != "0") then {
		echo "Falta variable $i en confv.empty";
	} fi;
	grep "$i" conf.sh > /dev/null
	if (test "$?" != "0") then {
		echo "Falta variable $i en conf.sh";
	} fi;
done;

noFuentes $DCONFSH

opera $DCONFSH/herram_confsh/comdist.mak herram_confsh/comdist.mak
opera $DCONFSH/herram_confsh/confaux.sh herram_confsh/confaux.sh
opera $DCONFSH/herram_confsh/misc.sh herram_confsh/misc.sh
opera $DCONFSH/herram_confsh/rtest.sh herram_confsh/rtest.sh


