# Reglas para desarrollar
# Basadas en reglas de dominio público de confsh
# http://www.pasosdejesus.org/devel/confsh

include Make.inc

SOURCES=

# Variables requeridas por comdist.mk

GENDIST=
# Dependencias para generar distribución

ACTHOST=ns2.pasosdejesus.org
ACTDIR=/var/www/users/mv/pasosdejesus/devel/confsh/
USER=$(USER)

GENACT=distcvs all 
# Reglas por emplear antes de actualizar sitio en Internet

FILESACT=$(PROYECTO)-$(PRY_VERSION).tar.gz 
# Archivos que se debe actualizar en sitio de Internet

all: 

# To crear distribución de fuentes y actualizar en Internet
include herram/comdist.mak


# Elimina hasta configuración
limpiadist: limpiamas
	rm -f confv.sh confv.ent Make.inc

# Elimina archivos generables
limpiamas: limpia
	rm -rf $(HTML_DIR)
	rm -rf $(PRINT_DIR)
	rm -f img/*.eps img/*.ps
	rm -f $(PROYECTO)-$(PRY_VERSION).tar.gz

# Elimina backups y archivos temporales
limpia:
	rm -f *bak *~ *tmp confaux.tmp confaux.sed 

.SUFFIXES: 

regr: 
	herram/misc.sh -t

instala:
	$(MKDIR) -p $(INSDATA)/
	$(CP) herram/*.mak herram/*.sh $(INSDATA)/
	$(SED) -e 's|rutaconfsh=".*"|rutaconfsh="$(INSDATA)"|g' herram/confaux.sh > $(INSDATA)/confaux.sh

desinstala:
	$(RM) -rf $(INSDATA)
